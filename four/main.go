package main

import (
	"fmt"
	"math"
)

// Range 134564-585159

func main() {
	counterOne := 0
	counterTwo := 0

	for i := 134564; i <= 585159; i++ {
		if checkValidOne(i) {
			counterOne++
		}

		if checkValidTwo(i) {
			counterTwo++
		}
	}

	fmt.Println(counterOne)
	fmt.Println(counterTwo)
}

func checkValidOne(nr int) bool {
	parts := parseParts(nr)

	return checkNotDecreasing(parts) && checkAdjacent(parts)
}

func checkValidTwo(nr int) bool {
	parts := parseParts(nr)

	return checkNotDecreasing(parts) && checkAdjacentTwo(parts)
}

func checkNotDecreasing(nr []int) bool {
	prev := nr[0]

	for _, n := range nr[1:] {
		if n < prev {
			return false
		}

		prev = n
	}

	return true
}

func checkAdjacent(nr []int) bool {
	prev := nr[0]

	for _, n := range nr[1:] {
		if n == prev {
			return true
		}

		prev = n
	}

	return false
}

func checkAdjacentTwo(nr []int) bool {
	for i := 0; i <= 9; i++ {
		adjacentCount := 1
		prev := -1

		for _, n := range nr {
			if n == i && prev == n {
				adjacentCount += 1
			}

			prev = n
		}

		if adjacentCount == 2 {
			return true
		}
	}

	return false
}

// Split an integer by dividing it and taking the remainder
func parseParts(nr int) (parts []int) {
	for i := 5; i >= 0; i-- {
		p := (nr / int(math.Pow(float64(10), float64(i)))) % 10
		parts = append(parts, p)
	}

	return
}