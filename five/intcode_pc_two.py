class IntcodeCPUV2:

    def __init__(self, codes):
        # Initialize the object by creating a ROM and copying it to Mem
        self.rom = codes.copy()

        # Initialize the base variables
        self.memory = self.rom.copy()
        self.ip = 0
        self.run_flag = True
        self.trace = []

        self.instructions = {
            1: self.add,
            2: self.product,
            3: self.inp,
            4: self.outp,
            5: self.jump_if_true,
            6: self.jump_if_false,
            7: self.lt,
            8: self.equals,
            99: self.halt
        }

    def set(self, pos, value):
        if len(self.memory) <= pos >= 0:
            self.add_trace("setting address {:d} to {:d} memory out of range",
                           pos, value)
            raise CPUException("Memory out of range")

        self.add_trace("setting address {:d} to {:d}", pos, value)
        self.memory[pos] = value

    def get(self, pos):
        if len(self.memory) <= pos >= 0:
            self.add_trace("retrieving address {:d} memory out of range", pos)
            raise CPUException("Memory out of range")

        value = self.memory[pos]
        self.add_trace("retrieving address {:d} value: {:d}", pos, value)

        return value

    def get_parameters(self, inst, modes):
        parameters = []

        for i in range(1, inst + 1):
            mode = modes[i - 1]

            if mode == 0:
                parameters.append(
                    self.get(self.get(self.ip + i))
                )
            elif mode == 1:
                parameters.append(self.get(self.ip + i))
            else:
                raise CPUException("Parameter mode doesn't exist")

        return parameters

    def get_instruction(self):
        opcode = self.get(self.ip)

        # Parse the opcode
        parsable = f"{opcode:05d}"

        modes = [int(a) for a in parsable[0:3]]
        modes.reverse()
        opcode = int(parsable[3:])

        # We check whether the given instruction exists
        if opcode in self.instructions:
            return self.instructions[opcode], modes
        else:
            self.add_trace("function for opcode {:d} not implemented", opcode)
            raise CPUException("Function not implemented")

    def add_trace(self, line, *args):
        if self.run_flag:
            self.trace.append(
                "Instruction at {:d}: {:s}".format(
                    self.ip, line.format(*args)
                )
            )

    def get_trace(self):
        t = ""
        for idx, trace in enumerate(self.trace):
            t += "#{:d} - {:s}\n".format(idx, trace)

        return t

    def panic(self, extra_info):
        self.halt()
        print(self.get_trace())
        print(extra_info)

    def reset(self):
        """
        Reset the PC
        """
        self.memory = self.rom.copy()
        self.ip = 0
        self.run_flag = True
        self.trace = []

    def run(self):
        """
        Run the complete program by processing the all the ints in order until
        the run flag is set to false
        :return:
        """
        while self.run_flag:
            self.process()

    def process(self):
        """
        This is where the magic happens
        :return: Bool
        """
        try:
            # Get the opcode at the current instruction pointer
            operation, modes = self.get_instruction()

            # Execute that function
            operation(modes)
        except CPUException as e:
            self.panic(e)
            return

    def add(self, modes):
        """
        Opcode: 1, Args: 2, Jumps: 4
        """
        modes[-1] = 1
        params = self.get_parameters(3, modes)
        n_a = params[0]
        n_b = params[1]
        store_at = params[2]

        self.add_trace("adding {:d} and {:d}", n_a, n_b)

        self.set(store_at, n_a + n_b)
        self.ip += 4

    def product(self, modes):
        """
        Opcode: 2, Args: 2, Jumps: 4
        """
        modes[-1] = 1
        params = self.get_parameters(3, modes)
        n_a = params[0]
        n_b = params[1]
        store_at = params[2]

        self.add_trace("multiplying {:d} and {:d}", n_a, n_b)

        self.set(store_at, n_a * n_b)
        self.ip += 4

    def halt(self, modes=None):
        """
        Opcode: 99, Args 0, Jumps: 0
        """
        self.add_trace("calling for halt - HALTING")
        self.run_flag = False

    def inp(self, modes):
        i = input("Program asks for input: ")

        if not i.isnumeric():
            raise CPUException("CPU only supports number input")

        i = int(i)
        store_at = self.get_parameters(1, [1])[0]
        self.add_trace("storing input {:d}", i, store_at)

        self.set(store_at, int(i))

        self.ip += 2

    def outp(self, modes):
        p = self.get_parameters(1, modes)[0]
        self.add_trace("outputting value {:d}", p)

        print(p)

        self.ip += 2

    def jump_if_true(self, modes):
        params = self.get_parameters(2, modes)
        n_a = params[0]
        n_b = params[1]

        if n_a != 0:
            self.add_trace("jumping to {:d}", n_b)
            self.ip = n_b
        else:
            self.ip += 3

    def jump_if_false(self, modes):
        params = self.get_parameters(2, modes)
        n_a = params[0]
        n_b = params[1]

        if n_a == 0:
            self.add_trace("jumping to {:d}", n_b)
            self.ip = n_b
        else:
            self.ip += 3

    def lt(self, modes):
        modes[-1] = 1

        params = self.get_parameters(3, modes)
        n_a = params[0]
        n_b = params[1]
        store_at = params[2]

        if n_a < n_b:
            self.set(store_at, 1)
        else:
            self.set(store_at, 0)

        self.ip += 4

    def equals(self, modes):
        modes[-1] = 1

        params = self.get_parameters(3, modes)
        n_a = params[0]
        n_b = params[1]
        store_at = params[2]

        if n_a == n_b:
            self.set(store_at, 1)
        else:
            self.set(store_at, 0)

        self.ip += 4


class CPUException(Exception):
    pass

