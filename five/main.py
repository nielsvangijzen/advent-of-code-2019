from five.intcode_pc_two import IntcodeCPUV2


if __name__ == "__main__":
    with open("input.txt", "r") as f:
        a = f.readline().strip()

    # Split all values
    values = a.split(",")

    # Cast all the strings to int because that's O P T I M I Z E D
    codes = [int(a) for a in values]

    pc = IntcodeCPUV2(codes)
    pc.run()
