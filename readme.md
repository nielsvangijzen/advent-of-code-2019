# Advent of code 2019
This repositories holds my code I use to complete the Advent of code exercises.
Use this repo as reference, but if you're doing the challenges as well 
(especially if you're doing it to learn) don't use this as cheat sheet. You'll
learn more by doing it yourself.

I tend to over engineer stuff like this so if you see complicated stuff that's
me being a tad too enthusiastic!
