from two.intcode_pc import IntcodeCPU


def part_one(opcodes):
    opcodes[1] = 12
    opcodes[2] = 2
    pc = IntcodeCPU(opcodes)
    pc.run()
    print(pc.get(0))


def part_two(opcodes):
    for noun in range(100):
        for verb in range(100):
            opcodes[1] = noun
            opcodes[2] = verb

            pc = IntcodeCPU(opcodes)
            pc.run()
            if pc.get(0) == 19690720:
                print(100 * noun + verb)
                return


def code_throwing_an_error():
    pc = IntcodeCPU([1, 0])
    pc.run()

    pc = IntcodeCPU([3, 0])
    pc.run()


if __name__ == "__main__":
    with open("input.txt", "r") as f:
        a = f.readline().strip()

    # Split all values
    values = a.split(",")

    # Cast all the strings to int because that's O P T I M I Z E D
    codes = [int(a) for a in values]

    # Execute both parts
    part_one(codes)
    part_two(codes)
    code_throwing_an_error()
