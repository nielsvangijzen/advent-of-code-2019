class IntcodeCPU:

    def __init__(self, codes):
        # Initialize the object by creating a ROM and copying it to Mem
        self.rom = codes.copy()

        # Initialize the base variables
        self.memory = self.rom.copy()
        self.ip = 0
        self.run_flag = True
        self.trace = []

        self.instructions = {
            1: self.add,
            2: self.product,
            99: self.halt
        }

    def set(self, pos, value):
        if len(self.memory) <= pos >= 0:
            self.add_trace("setting address {:d} to {:d} memory out of range",
                           pos, value)
            raise CPUException("Memory out of range")

        self.add_trace("setting address {:d} to {:d}", pos, value)
        self.memory[pos] = value

    def get(self, pos):
        if len(self.memory) <= pos >= 0:
            self.add_trace("retrieving address {:d} memory out of range", pos)
            raise CPUException("Memory out of range")

        value = self.memory[pos]
        self.add_trace("retrieving address {:d} value: {:d}", pos, value)

        return value

    def get_instruction(self):
        opcode = self.get(self.ip)

        # We check whether the given instruction exists
        if opcode in self.instructions:
            return self.instructions[opcode]
        else:
            self.add_trace("function for opcode {:d} not implemented", opcode)
            raise CPUException("Function not implemented")

    def add_trace(self, line, *args):
        if self.run_flag:
            self.trace.append(
                "Instruction at {:d}: {:s}".format(
                    self.ip, line.format(*args)
                )
            )

    def get_trace(self):
        t = ""
        for idx, trace in enumerate(self.trace):
            t += "#{:d} - {:s}\n".format(idx, trace)

        return t

    def panic(self, extra_info):
        self.halt()
        print(self.get_trace())
        print(extra_info)

    def reset(self):
        """
        Reset the PC
        """
        self.memory = self.rom.copy()
        self.ip = 0
        self.run_flag = True
        self.trace = []

    def run(self):
        """
        Run the complete program by processing the all the ints in order until
        the run flag is set to false
        :return:
        """
        while self.run_flag:
            self.process()

    def process(self):
        """
        This is where the magic happens
        :return: Bool
        """
        try:
            # Get the opcode at the current instruction pointer
            operation = self.get_instruction()

            # Execute that function
            operation()
        except CPUException as e:
            self.panic(e)
            return

    def add(self):
        """
        Opcode: 1, Args: 2, Jumps: 4
        """
        n_a = self.get(self.get(self.ip + 1))
        n_b = self.get(self.get(self.ip + 2))
        store_at = self.get(self.ip + 3)

        self.set(store_at, n_a + n_b)
        self.ip += 4

    def product(self):
        """
        Opcode: 2, Args: 2, Jumps: 4
        """
        n_a = self.get(self.get(self.ip + 1))
        n_b = self.get(self.get(self.ip + 2))
        store_at = self.get(self.ip + 3)

        self.set(store_at, n_a * n_b)
        self.ip += 4

    def halt(self):
        """
        Opcode: 99, Args 0, Jumps: 0
        """
        self.add_trace("calling for halt - HALTING")
        self.run_flag = False


class CPUException(Exception):
    pass
