import math


def calc_fuel(m):
    """
    This function calculates the required fuel recursively
    :param m:
    :return:
    """
    fuel = 0
    floored = math.floor(m / 3)
    fuel_to_add = floored - 2

    # We check whether the fuel to add is bigger than zero
    # If so we've added fuel and gained more wat so we need more fuel
    # We then call this function again to account for that extra weight until
    # The fuel_to_add becomes so little that it becomes zero or negative.
    # This is what we call recursion
    if fuel_to_add > 0:
        fuel += fuel_to_add
        fuel += calc_fuel(fuel_to_add)

    return fuel


if __name__ == "__main__":
    total_fuel = 0

    # Parse all the lines
    with open("input.txt", "r") as f:
        modules = f.readlines()

    for module_mass in modules:
        # Strip the break from the input and cast to int
        cleaned = int(module_mass.strip())

        # Calculate the fuel needed for this module
        total_fuel += calc_fuel(cleaned)

    print(total_fuel)
