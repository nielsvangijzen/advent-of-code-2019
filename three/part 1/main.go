package main

import (
	"fmt"
	"io/ioutil"
	"math"
	"os"
	"strconv"
	"strings"
)

const width = 30000
const height = 30000

func main() {
	manhattan()
}

func manhattan() {
	lines := openInput()
	lineA := lines[0]
	lineB := lines[1]

	gridA := createGrid(lineA)
	gridB := createGrid(lineB)

	middleX := width / 2
	middleY := height / 2

	currentShortest := 50000

	for y := 0; y < height; y++ {
		for x := 0; x < width; x++ {
			if gridA[y][x] && gridB[y][x] {
				if x == middleX && y == middleY {
					continue
				}

				fmt.Printf("{x: %d, y: %d},\n", x, y)

				distance := taxicab(middleX, middleY, x, y)
				if distance < currentShortest {
					currentShortest = distance
					//fmt.Printf("x: %d y: %d\n", x, y)
					//fmt.Println(distance)
				}
			}
		}
	}
}

func taxicab(midX int, midY int, x int, y int) (distance int) {

	distance += int(math.Abs(float64(midX - x)))
	distance += int(math.Abs(float64(midY - y)))

	return
}

func createGrid(line string) (grid [][]bool) {
	arr := strings.Split(line, ",")

	grid = make([][]bool, height)
	for i := 0; i < height; i++ {
		grid[i] = make([]bool, width)
	}

	x := width / 2
	y := height / 2

	for _, element := range arr {
		direction := element[0:1]
		steps, err := strconv.Atoi(element[1:])

		if err != nil {
			os.Exit(5)
		}

		grid[y][x] = true

		switch direction {
		case "R":
			for i := 0; i < steps; i++ {
				x += 1
				grid[y][x] = true
			}
			break
		case "L":
			for i := 0; i < steps; i++ {
				x -= 1
				grid[y][x] = true
			}
			break
		case "U":
			for i := 0; i < steps; i++ {
				y -= 1
				grid[y][x] = true
			}
			break
		case "D":
			for i := 0; i < steps; i++ {
				y += 1
				grid[y][x] = true
			}
			break
		}
	}

	return
}

func openInput() []string {
	file, err := os.Open("../input.txt")
	if err != nil {
		fmt.Errorf("Couldn't open file")
		os.Exit(4)
	}

	b, err := ioutil.ReadAll(file)

	text := string(b)
	return strings.Split(text, "\n")
}
