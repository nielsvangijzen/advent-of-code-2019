package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"strconv"
	"strings"
)

const width = 30000
const height = 30000

type Point struct {
	x int
	y int
}

var knownIntersections = []Point{
	{x: 16478, y: 14374},
	{x: 16853, y: 14506},
	{x: 12353, y: 14517},
	{x: 17102, y: 14574},
	{x: 12206, y: 14610},
	{x: 17102, y: 14832},
	{x: 15399, y: 15000},
	{x: 15645, y: 15000},
	{x: 12206, y: 15042},
	{x: 12206, y: 15043},
	{x: 12206, y: 15128},
	{x: 12206, y: 15265},
	{x: 16411, y: 15429},
	{x: 12237, y: 15430},
	{x: 12502, y: 15430},
	{x: 15718, y: 15590},
	{x: 15844, y: 15590},
	{x: 16292, y: 15645},
	{x: 16360, y: 15645},
	{x: 15718, y: 15708},
	{x: 16360, y: 15736},
	{x: 17029, y: 16191},
	{x: 17364, y: 16435},
	{x: 17903, y: 16833},
	{x: 17903, y: 16856},
	{x: 17903, y: 16929},
	{x: 17331, y: 17037},
	{x: 17870, y: 17037},
	{x: 17329, y: 17130},
	{x: 17903, y: 17150},
	{x: 18280, y: 17150},
	{x: 19003, y: 17225},
	{x: 19120, y: 17225},
	{x: 17329, y: 17433},
	{x: 17329, y: 17538},
	{x: 16353, y: 17664},
	{x: 17329, y: 17736},
	{x: 14375, y: 17766},
	{x: 14871, y: 17794},
	{x: 16658, y: 17800},
	{x: 17031, y: 17800},
	{x: 17056, y: 17800},
	{x: 17112, y: 17800},
	{x: 17179, y: 17800},
	{x: 14503, y: 17875},
	{x: 14662, y: 17875},
	{x: 17688, y: 18051},
	{x: 17696, y: 18051},
	{x: 17898, y: 18051},
	{x: 17523, y: 18066},
	{x: 17523, y: 18221},
	{x: 17880, y: 18427},
	{x: 17880, y: 18542},
	{x: 17880, y: 18793},
}

func main() {
	lines := openInput()
	lineA := lines[0]
	lineB := lines[1]

	gridA := createGrid(lineA)
	gridB := createGrid(lineB)

	//middleX := width / 2
	//middleY := height / 2

	currentShortest := 500000

	//lenA := len(gridA)
	//lenB := len(gridB)

	fmt.Println(len(knownIntersectionsTest))

	for _, point := range knownIntersections {
		indexA := pointIndex(point, gridA) + 1
		indexB := pointIndex(point, gridB) + 1

		distance := indexA + indexB

		if currentShortest > distance {
			currentShortest = distance

			fmt.Println(distance)
		}
	}
}

func pointIndex(point Point, arr []Point) int {
	for index, p := range arr {
		if point.x == p.x && point.y == p.y {
			return index
		}
	}

	return -1
}

func createGrid(line string) (grid []Point) {
	arr := strings.Split(line, ",")

	x := width / 2
	y := height / 2

	for _, element := range arr {
		direction := element[0:1]
		steps, err := strconv.Atoi(element[1:])

		if err != nil {
			os.Exit(5)
		}

		switch direction {
		case "R":
			for i := 0; i < steps; i++ {
				x += 1
				grid = append(grid, Point{x: x, y: y})
			}
			break
		case "L":
			for i := 0; i < steps; i++ {
				x -= 1
				grid = append(grid, Point{x: x, y: y})
			}
			break
		case "U":
			for i := 0; i < steps; i++ {
				y -= 1
				grid = append(grid, Point{x: x, y: y})
			}
			break
		case "D":
			for i := 0; i < steps; i++ {
				y += 1
				grid = append(grid, Point{x: x, y: y})
			}
			break
		}
	}

	return
}

func openInput() []string {
	file, err := os.Open("../input.txt")
	if err != nil {
		fmt.Errorf("Couldn't open file")
		os.Exit(4)
	}

	b, err := ioutil.ReadAll(file)

	text := string(b)
	return strings.Split(text, "\n")
}
