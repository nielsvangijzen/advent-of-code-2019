package main

var rowNum = []int{-1, 0, 0, 1}
var colNum = []int{0, -1, 1, 0}

type queueNode struct {
	x    int
	y    int
	dist int
}

type FIFO struct {
	queue []interface{}
}

func NewQueue() *FIFO {
	return &FIFO{
		queue: make([]interface{}, 0),
	}
}

func (f *FIFO) Push(node interface{}) {
	f.queue = append(f.queue, node)
}

func (f *FIFO) Empty() bool {
	return len(f.queue) == 0
}

func (f *FIFO) Front() interface{} {
	if len(f.queue) == 0 {
		return nil
	}

	node := f.queue[0]
	f.queue[0] = nil
	f.queue = f.queue[1:]

	return node
}

func BFS(grid [][]bool, srcX int, srcY int, destX int, destY int) (distance int) {
	visited := make([][]bool, height)
	for i := 0; i < height; i++ {
		visited[i] = make([]bool, width)
	}

	visited[srcY][srcX] = true
	queue := NewQueue()
	qSrc := queueNode{
		x:    srcX,
		y:    srcY,
		dist: 0,
	}

	queue.Push(qSrc)

	for !queue.Empty() {
		node := queue.Front()
		curr := node.(queueNode)

		if curr.x == destX && curr.y == destY {
			return curr.dist
		}

		for i := 0; i < 4; i++ {
			x := curr.x + colNum[i]
			y := curr.y + rowNum[i]

			if grid[y][x] && !visited[y][x] {
				visited[y][x] = true
				queue.Push(queueNode{
					x:    x,
					y:    y,
					dist: curr.dist + 1,
				})
			}
		}
	}

	return -1
}
